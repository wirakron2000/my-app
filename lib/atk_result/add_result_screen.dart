import 'package:flutter/material.dart';
import 'package:my_app/export_file/service.dart';
import 'package:my_app/export_file/widget.dart';

class AddResultScreen extends StatefulWidget {
  const AddResultScreen({Key? key}) : super(key: key);

  @override
  State<AddResultScreen> createState() => _AddResultScreenState();
}

class _AddResultScreenState extends State<AddResultScreen> {
  @override
  void initState() {
    getCurrentPosition();
    setState(() {});
    super.initState();
  }

  @override
  void dispose() {
    imageFile = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (imageFile == null) {
      Navigator.of(context).pop();
    }
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          //automaticallyImplyLeading: false,
          backgroundColor: const Color.fromARGB(255, 69, 196, 194),
          centerTitle: true,
          title: const Text(
            'เพิ่มผลการบันทึก',
            style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
          ),
        ),
        body: Container(
          padding: const EdgeInsets.all(5),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Colors.blueAccent.withOpacity(0.80),
                Colors.lightBlueAccent.withOpacity(0.70)
              ],
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
            ),
          ),
          child: Container(
            width: double.infinity,
            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
            decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.45),
                borderRadius: const BorderRadius.all(Radius.circular(5))),
            child: Column(
              children: [
                Container(
                  width: double.infinity,
                  margin: const EdgeInsets.only(bottom: 5),
                  decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.45),
                      borderRadius: const BorderRadius.all(Radius.circular(5))),
                  child: const Center(
                    child: Text(
                      'รายละเอียด',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
                    ),
                  ),
                ),
                Container(
                  width: double.infinity,
                  padding: const EdgeInsets.all(10),
                  decoration: const BoxDecoration(
                      color: Colors.grey,
                      borderRadius: BorderRadius.all(Radius.circular(5))),
                  child: const Center(
                      child: Text(
                    'ยังไม่ได้รับการตรวจสอบ',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  )),
                ),
                Expanded(
                  child: Container(
                    width: double.infinity,
                    margin: const EdgeInsets.symmetric(vertical: 5),
                    padding: const EdgeInsets.all(5),
                    decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(5))),
                    child: SingleChildScrollView(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Column(
                        children: [
                          BoxTextField(
                              label: 'สถานที่',
                              controller: address,
                              enable: false),
                          const DropDownAtk(),
                          Container(
                              margin: const EdgeInsets.all(5),
                              padding: const EdgeInsets.all(5),
                              width: double.infinity,
                              color: Colors.grey,
                              height: 400,
                              child: imageFile != null
                                  ? Image.file(
                                      imageFile!,
                                      width: double.infinity,
                                      fit: BoxFit.cover,
                                    )
                                  : Container()),
                        ],
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  child: Container(
                    width: double.infinity,
                    padding: const EdgeInsets.all(10),
                    decoration: const BoxDecoration(
                        color: Color(0xFF6488E4),
                        borderRadius: BorderRadius.all(Radius.circular(5))),
                    child: const Center(
                        child: Text(
                      'ยืนยันผลการบันทึก',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 20),
                    )),
                  ),
                  onTap: () async {
                    await addResult().then((value) {
                      setState(() {
                        newResult = true;
                        clearPosition();
                      });
                      Navigator.of(context)
                          .pushNamedAndRemoveUntil('/result', (route) => false);
                    });
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
