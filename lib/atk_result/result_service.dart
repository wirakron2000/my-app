import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_app_check/firebase_app_check.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:my_app/export_file/service.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';

final appCheck = FirebaseAppCheck.instance;

String? resultPath;
String? verifyPath;
String? entryPath;
String? imageUrl;

DateFormat dateFormat = DateFormat('d-MM-y, hh:mm:ss a');

bool newResult = false;
bool visible = false;
bool changeShow = true;
bool? ceck;

TextEditingController address = TextEditingController();
TextEditingController dateTime = TextEditingController();
TextEditingController userName = TextEditingController();
TextEditingController atkBrand = TextEditingController();

clearResultValue() {
  address.text = '';
  dateTime.text = '';
  userName.text = '';
  atkBrand.text = '';
  resultPath = null;
  imageUrl = null;
  newResult = false;
  visible = false;
  changeShow = true;
}

Future<void> addResult() async {
  String? url = await uploadImage(imageFile!);
  newResult = true;
  await userRef
      .doc(currentUser!.email)
      .collection('atk_result')
      .add({
        'atk_brand': atkRef.doc(currentAtk),
        'address': GeoPoint(position!.latitude, position!.longitude),
        'date_time': FieldValue.serverTimestamp(),
        'image': url,
        'name': userRef.doc(currentUser!.email.toString()),
      })
      .then((value) => debugPrint('Success to add result.'))
      .catchError((error) => debugPrint('Failed to add result: $error'));
}

Future<String?> uploadImage(File image) async {
  try {
    String fileName = basename(image.path);
    Reference storageRef =
        FirebaseStorage.instance.ref().child('upload/$fileName');
    await storageRef.putFile(image);
    return await storageRef.getDownloadURL();
  } catch (e) {
    debugPrint('Error: |- $e -|');
    return null;
  }
}

getcertifierList() {
  return FirebaseFirestore.instance.doc(resultPath!);
}

Future<void> getResult() async {
  try {
    await FirebaseFirestore.instance.doc(resultPath!).get().then((value) {
      imageUrl = value.get('image');
      GeoPoint geo = value.get('address');
      getPlacemarks(geo.latitude, geo.longitude);
      address.text = getAddress();
      dateTime.text = dateFormat.format(value.get('date_time').toDate());
      DocumentReference<Map<String, dynamic>> atk = value.get('atk_brand');
      atk.get().then((value) {
        atkBrand.text = value.get('brand');
      });
      DocumentReference<Map<String, dynamic>> name = value.get('name');
      userPath = name.path;
      name.get().then((value) {
        userName.text = value.get('name').toString();
      });
      debugPrint('Success to get result.');
    });
  } on FirebaseFirestore catch (e) {
    debugPrint('Failed to get result: $e');
  }
}

Future<void> getLatestResult() async {
  await userRef
      .doc(currentUser!.email)
      .collection('atk_result')
      .orderBy('date_time', descending: false)
      .get()
      .then((value) {
    resultPath = value.docs.last.reference.path.toString();
    getResult();
  });
  debugPrint('|$resultPath|');
}

Future<void> addResultApprove(bool approve) async {
  await FirebaseFirestore.instance
      .doc(userPath!)
      .collection('approve')
      .add({
        'certifier': userRef.doc(currentUser?.email),
        'date_time': FieldValue.serverTimestamp(),
        'result': FirebaseFirestore.instance.doc(resultPath!),
        'approve': approve
      })
      .then((value) => debugPrint('Success to add approve.'))
      .catchError((err) => debugPrint('Failed to add approve'));
  String? approvePath;
  await FirebaseFirestore.instance
      .doc(userPath!)
      .collection('approve')
      .orderBy('date_time', descending: false)
      .get()
      .then((value) {
        approvePath = value.docs.last.reference.path;
      })
      .then((value) => debugPrint('Success to get newest approve path.'))
      .catchError((err) => debugPrint('Failed to get newest approve path'));

  updateCerifier(approvePath!);
}

Future<void> updateCerifier(String path) async {
  await FirebaseFirestore.instance
      .doc(resultPath!)
      .update({
        'certifier':
            FieldValue.arrayUnion([FirebaseFirestore.instance.doc(path)]),
      })
      .then((value) => debugPrint('Success to update result.'))
      .catchError((error) => debugPrint('Failed to update result: $error'));
}

Future<void> addEntryHistory(bool permission) async {
  await FirebaseFirestore.instance
      .doc(userPath!)
      .collection('entry_history')
      .add({
        'certifier': userRef.doc(currentUser?.email),
        'date_time': FieldValue.serverTimestamp(),
        'result': FirebaseFirestore.instance.doc(resultPath!),
        'permission': permission
      })
      .then((value) => debugPrint('Success to add entry history.'))
      .catchError((err) => debugPrint('Failed to add entry history'));
  String? entryPath;
  await FirebaseFirestore.instance
      .doc(userPath!)
      .collection('entry_history')
      .orderBy('date_time', descending: false)
      .get()
      .then((value) {
        entryPath = value.docs.last.reference.path;
      })
      .then((value) => debugPrint('Success to get newest entry path.'))
      .catchError((err) => debugPrint('Failed to get newest entry path'));

  updateEntryHistory(entryPath!);
}

Future<void> updateEntryHistory(String path) async {
  await FirebaseFirestore.instance
      .doc(resultPath!)
      .update({
        'entry_history':
            FieldValue.arrayUnion([FirebaseFirestore.instance.doc(path)]),
      })
      .then((value) => debugPrint('Success to update result.'))
      .catchError((error) => debugPrint('Failed to update result: $error'));
}

Future<void> deleteResult() async {
  await FirebaseFirestore.instance
      .doc(resultPath!)
      .delete()
      .then((value) => debugPrint('Success to delete result.'))
      .catchError((error) => debugPrint('Failed to delete result: $error'));
}

Stream<QuerySnapshot> getAllResultStream() {
  return userRef
      .doc(currentUser!.email)
      .collection('atk_result')
      .orderBy('date_time', descending: true)
      .snapshots();
}

Stream<QuerySnapshot> getNewestResultStream() {
  return userRef
      .doc(currentUser!.email)
      .collection('atk_result')
      .orderBy('date_time', descending: true)
      .limit(6)
      .snapshots();
}

Stream<QuerySnapshot> getCertifierStream() {
  return userRef
      .doc(currentUser?.email)
      .collection('approve')
      .orderBy('date_time', descending: false)
      .snapshots();
}

Stream<QuerySnapshot> getEntryStream() {
  return userRef
      .doc(currentUser?.email)
      .collection('entry_history')
      .orderBy('date_time', descending: false)
      .snapshots();
}
