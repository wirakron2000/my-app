import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:my_app/export_file/service.dart';
import 'package:my_app/export_file/widget.dart';

class AllResultScreen extends StatefulWidget {
  const AllResultScreen({Key? key}) : super(key: key);

  @override
  State<AllResultScreen> createState() => _AllResultScreenState();
}

class _AllResultScreenState extends State<AllResultScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: const CustomNavbar(),
        backgroundColor: Colors.blueAccent,
        appBar: AppBar(
          backgroundColor: const Color.fromARGB(255, 69, 196, 194),
          centerTitle: true,
          title: const Text(
            'ประวัติผลบันทึก ATK',
            style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
          ),
        ),
        body: Container(
          padding: const EdgeInsets.all(5),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Colors.blueAccent.withOpacity(0.80),
                Colors.lightBlueAccent.withOpacity(0.70)
              ],
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
            ),
          ),
          child: Container(
            padding: const EdgeInsets.all(5),
            decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.45),
                borderRadius: const BorderRadius.all(Radius.circular(10))),
            child: SingleChildScrollView(
              child: StreamBuilder(
                stream: getAllResultStream(),
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (snapshot.data?.size == 0) {
                    return const Center(
                      child: Text('ไม่มีผลการบันทึก ATK'),
                    );
                  }
                  if (snapshot.hasError) {
                    return const Text('Something went wrong');
                  }
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Container(
                      margin: const EdgeInsets.symmetric(
                          horizontal: 100, vertical: 10),
                      child: const CircularProgressIndicator(strokeWidth: 5),
                    );
                  }
                  return Column(
                    children: snapshot.data!.docs.map((document) {
                      return ResultList(data: document);
                    }).toList(),
                  );
                },
              ),
            ),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton.large(
          backgroundColor:
              const Color.fromARGB(150, 59, 164, 162).withOpacity(0.8),
          child: const Icon(Icons.qr_code_scanner_outlined, size: 65),
          onPressed: () {
            qrNavi = 'result';
            Navigator.of(context).pushNamed('/qr_scan');
            debugPrint('Qr scan');
          },
        ),
        bottomNavigationBar: customBottomBar(context),
      ),
    );
  }
}

class ResultList extends StatelessWidget {
  const ResultList({
    Key? key,
    required this.data,
  }) : super(key: key);

  final QueryDocumentSnapshot data;

  @override
  Widget build(BuildContext context) {
    GeoPoint geo = data.get('address');
    getPlacemarks(geo.latitude, geo.longitude);
    DocumentReference atk = data.get('atk_brand');
    DocumentReference user = data.get('name');
    List certifier = <DocumentReference>[];
    try {
      certifier = data.get('certifier');
    } catch (e) {
      debugPrint('$e');
    }
    return Container(
      decoration: BoxDecoration(
        color: Colors.cyan.shade100.withOpacity(0.5),
        borderRadius: BorderRadius.circular(5),
      ),
      margin: const EdgeInsets.all(2),
      child: FutureBuilder(
        future: Future.wait([atk.get(), user.get()]),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            return GestureDetector(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    height: 100,
                    decoration: BoxDecoration(
                      color: Colors.blueGrey.withOpacity(0.5),
                      borderRadius: BorderRadius.circular(5),
                    ),
                    margin: const EdgeInsets.all(5),
                    padding: const EdgeInsets.all(5),
                    child: Center(
                      child: CachedNetworkImage(
                        placeholder: (context, url) =>
                            const CircularProgressIndicator(),
                        errorWidget: (context, url, error) =>
                            const Icon(Icons.error),
                        imageUrl: data.get('image'),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.6),
                          borderRadius:
                              const BorderRadius.all(Radius.circular(5))),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: double.infinity,
                            padding: const EdgeInsets.all(5),
                            decoration: BoxDecoration(
                                color: certifier.isNotEmpty
                                    ? const Color.fromARGB(255, 70, 182, 93)
                                    : const Color.fromARGB(255, 225, 55, 55),
                                borderRadius: BorderRadius.circular(5)),
                            child: Center(
                                child: Text(
                              certifier.isNotEmpty
                                  ? 'มีผลการยืนยัน'
                                  : 'ยังไม่ได้รับการยืนยัน',
                              style: const TextStyle(
                                  color: Colors.white,
                                  fontSize: 17,
                                  fontWeight: FontWeight.bold),
                            )),
                          ),
                          Container(
                            padding: const EdgeInsets.symmetric(horizontal: 5),
                            child: Text(
                              'ยี่ห้อ ATK : ${snapshot.data[0]['brand']}',
                              style: const TextStyle(fontSize: 17),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.symmetric(horizontal: 5),
                            child: Text(
                              'เวลาที่บันทึก : ${dateFormat.format(data.get('date_time').toDate())}',
                              style: const TextStyle(fontSize: 17),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.symmetric(horizontal: 5),
                            child: Text(
                              'สถานที่ : ${getAddress()}',
                              style: const TextStyle(fontSize: 17),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              onTap: () async {
                resultPath = data.reference.path;
                debugPrint(data.id);
                Navigator.of(context).pushNamed('/result');
              },
            );
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const CircularProgressIndicator();
          }
          return Container();
        },
      ),
    );
  }
}
