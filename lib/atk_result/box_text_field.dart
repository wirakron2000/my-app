import 'package:flutter/material.dart';

class BoxTextField extends StatelessWidget {
  final String label;
  final TextEditingController controller;
  final bool enable;

  const BoxTextField({
    super.key,
    required this.label,
    required this.controller,
    required this.enable,
  });

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      style: const TextStyle(
          color: Colors.black87, overflow: TextOverflow.ellipsis),
      minLines: 1,
      maxLines: 1,
      enabled: enable,
      decoration: InputDecoration(
          labelText: label,
          labelStyle: const TextStyle(color: Colors.black45),
          focusedBorder: const UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black)),
          border: const UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.grey))),
    );
  }
}
