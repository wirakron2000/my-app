import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:my_app/export_file/service.dart';
import 'package:my_app/export_file/widget.dart';

class VerifyResultScreen extends StatefulWidget {
  const VerifyResultScreen({Key? key}) : super(key: key);

  @override
  State<VerifyResultScreen> createState() => _VerifyResultScreenState();
}

class _VerifyResultScreenState extends State<VerifyResultScreen> {
  List _certifierList = <DocumentReference>[];
  DocumentReference certifierRef = getcertifierList();

  @override
  void initState() {
    setState(() {
      getResult();
      getCertifier();
    });
    super.initState();
  }

  getCertifier() async {
    await certifierRef.get().then((value) {
      try {
        setState(() {
          _certifierList = value.get('certifier');
        });
      } catch (e) {
        debugPrint('$e');
      }
    });
  }

  @override
  void dispose() {
    clearResultValue();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: const Color.fromARGB(255, 69, 196, 194),
          centerTitle: true,
          title: const Text(
            'ผลการบันทึก',
            style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
          ),
        ),
        body: Container(
          padding: const EdgeInsets.all(5),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Colors.blueAccent.withOpacity(0.80),
                Colors.lightBlueAccent.withOpacity(0.70)
              ],
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
            ),
          ),
          child: Container(
            padding: const EdgeInsets.all(5),
            decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.45),
                borderRadius: const BorderRadius.all(Radius.circular(5))),
            child: Column(
              children: [
                if (_certifierList.isNotEmpty)
                  CertifierList(listCertifier: _certifierList)
                else
                  Container(
                    width: double.infinity,
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        color: Colors.grey,
                        border: Border.all(width: 2, color: Colors.black38)),
                    child: const Center(
                        child: Text(
                      'ยังไม่ได้รับการยืนยัน',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 20),
                    )),
                  ),
                Expanded(
                  child: Container(
                    width: double.infinity,
                    margin: const EdgeInsets.symmetric(vertical: 5),
                    padding: const EdgeInsets.all(5),
                    decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(5))),
                    child: SingleChildScrollView(
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      child: Column(
                        children: [
                          Container(
                            width: double.infinity,
                            padding: const EdgeInsets.all(5),
                            decoration: const BoxDecoration(
                                color: Color.fromARGB(255, 25, 184, 181),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(3))),
                            child: const Center(
                              child: Text(
                                'รายละเอียด',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 25,
                                    color: Colors.white),
                              ),
                            ),
                          ),
                          BoxTextField(
                              label: 'ชื่อ-นามสกุล',
                              controller: userName,
                              enable: false),
                          BoxTextField(
                              label: 'สถานที่บันทึก',
                              controller: address,
                              enable: false),
                          BoxTextField(
                              label: 'เวลาที่บันทึก',
                              controller: dateTime,
                              enable: false),
                          BoxTextField(
                              label: 'ยี่ห้อ ATK',
                              controller: atkBrand,
                              enable: false),
                          const SizedBox(height: 5),
                          Row(
                            children: [
                              Expanded(
                                child: GestureDetector(
                                  child: Container(
                                    padding: const EdgeInsets.all(5),
                                    decoration: const BoxDecoration(
                                        color: Color(0xFF6488E4),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5))),
                                    child: const Center(
                                        child: Text('ดูประวัติการเข้าสถานที่',
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold,
                                            ))),
                                  ),
                                  onTap: () async {
                                    setState(() {
                                      if (visible == false &&
                                          changeShow == false) {
                                        changeShow = true;
                                      } else {
                                        changeShow = true;
                                        visible = !visible;
                                      }
                                    });
                                  },
                                ),
                              ),
                              const SizedBox(width: 5),
                              Expanded(
                                child: GestureDetector(
                                  child: Container(
                                    padding: const EdgeInsets.all(5),
                                    decoration: const BoxDecoration(
                                        color: Color(0xFF6488E4),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5))),
                                    child: const Center(
                                        child: Text('ดูภาพถ่าย',
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold,
                                            ))),
                                  ),
                                  onTap: () {
                                    setState(() {
                                      if (visible == false &&
                                          changeShow == true) {
                                        changeShow = false;
                                      } else {
                                        changeShow = false;
                                        visible = !visible;
                                      }
                                      // visible = !visible;
                                    });
                                  },
                                ),
                              )
                            ],
                          ),
                          AnimatedOpacity(
                            opacity: visible ? 0.0 : 1.0,
                            duration: const Duration(seconds: 1),
                            child: Container(
                              child: changeShow
                                  ? Container(
                                      padding: const EdgeInsets.all(5),
                                      decoration: BoxDecoration(
                                          color: Colors.white.withOpacity(0.45),
                                          borderRadius: const BorderRadius.all(
                                              Radius.circular(10))),
                                      child:
                                          const Text('ประวัติการเข้าสถานที่'),
                                    )
                                  : Container(
                                      margin: const EdgeInsets.all(5),
                                      padding: const EdgeInsets.all(5),
                                      height: visible ? 0 : null,
                                      color: Colors.grey,
                                      child: CachedNetworkImage(
                                          placeholder: (context, url) =>
                                              const CircularProgressIndicator(),
                                          errorWidget: (context, url, error) =>
                                              const Icon(Icons.error),
                                          imageUrl: imageUrl != null
                                              ? imageUrl!
                                              : ''),
                                    ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      child: GestureDetector(
                        child: Container(
                          width: double.infinity,
                          padding: const EdgeInsets.all(10),
                          decoration: const BoxDecoration(
                              color: Color.fromARGB(255, 201, 53, 53),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                          child: const Center(
                              child: Text(
                            'ไม่รับรอง',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          )),
                        ),
                        onTap: () {
                          setState(() {
                            clearQrPath();
                          });
                          addResultApprove(false);
                          Navigator.of(context).pushNamedAndRemoveUntil(
                              '/home', (route) => false);
                        },
                      ),
                    ),
                    const SizedBox(width: 5),
                    Expanded(
                      child: GestureDetector(
                        child: Container(
                          width: double.infinity,
                          padding: const EdgeInsets.all(10),
                          decoration: const BoxDecoration(
                              color: Color.fromARGB(255, 70, 182, 93),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5))),
                          child: const Center(
                              child: Text(
                            'รับรอง',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 20),
                          )),
                        ),
                        onTap: () {
                          addResultApprove(true);
                          setState(() {
                            clearQrPath();
                          });
                          Navigator.of(context).pushNamedAndRemoveUntil(
                              '/home', (route) => false);
                        },
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
