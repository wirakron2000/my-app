import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:my_app/export_file/service.dart';

DocumentReference? company;
bool hasCompany = false;

getCompany() async {
  await userRef.doc(currentUser!.email).get().then((value) {
    try {
      company = value.get('company');
      hasCompany = true;
      debugPrint(company!.path);
    } on StateError catch (err) {
      debugPrint('$err');
    }
    debugPrint('$hasCompany');
  });
}

bool getHasCompany() {
  return hasCompany;
}

Stream<QuerySnapshot> getLocationStream() {
  return FirebaseFirestore.instance
      .doc(company!.path)
      .collection('location')
      .orderBy('name')
      .snapshots();
}
