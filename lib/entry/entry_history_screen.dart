import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:my_app/export_file/service.dart';
import 'package:my_app/export_file/widget.dart';

class EntryHistoryScreen extends StatefulWidget {
  const EntryHistoryScreen({Key? key}) : super(key: key);

  @override
  State<EntryHistoryScreen> createState() => _EntryHistoryScreenState();
}

class _EntryHistoryScreenState extends State<EntryHistoryScreen> {
  @override
  void initState() {
    setState(() {
      getCompany();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: const CustomNavbar(),
        backgroundColor: Colors.blueAccent,
        appBar: AppBar(
          backgroundColor: const Color.fromARGB(255, 69, 196, 194),
          centerTitle: true,
          title: const Text(
            'ประวัติการเข้าสถานที่',
            style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
          ),
        ),
        body: Container(
          padding: const EdgeInsets.all(5),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Colors.blueAccent.withOpacity(0.80),
                Colors.lightBlueAccent.withOpacity(0.70)
              ],
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
            ),
          ),
          child: Container(
            padding: const EdgeInsets.all(5),
            decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.45),
                borderRadius: const BorderRadius.all(Radius.circular(10))),
            child: SingleChildScrollView(
              child: StreamBuilder(
                stream: getLocationStream(),
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (snapshot.hasError) {
                    return const Text('Something went wrong');
                  }
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const Text('Loading..');
                  }
                  return Column(
                    children: snapshot.data!.docs.map((document) {
                      return Container(
                        decoration: BoxDecoration(
                          color: Colors.cyan.shade100.withOpacity(0.5),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        margin: const EdgeInsets.all(5),
                        child: GestureDetector(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Expanded(
                                child: Container(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 5),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                          'สถานที่ : ${(document.get('name'))}',
                                          style: const TextStyle(fontSize: 17)),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          onTap: () async {
                            debugPrint(document.id);
                          },
                        ),
                      );
                    }).toList(),
                  );
                },
              ),
            ),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton.large(
          backgroundColor:
              const Color.fromARGB(150, 59, 164, 162).withOpacity(0.8),
          child: const Icon(Icons.qr_code_scanner_outlined, size: 65),
          onPressed: () {
            qrNavi = 'result';
            Navigator.of(context).pushNamed('/qr_scan');
            debugPrint('Qr scan');
          },
        ),
        bottomNavigationBar: customBottomBar(context),
      ),
    );
  }
}
