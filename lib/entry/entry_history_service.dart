import 'package:my_app/export_file/service.dart';

getEntryHistoryStream() {
  return userRef
      .doc(currentUser!.email)
      .collection('entry_history')
      .orderBy('date_time', descending: true)
      .snapshots();
}
