import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:my_app/export_file/service.dart';

class HistoryList extends StatelessWidget {
  final List listHistory;
  const HistoryList({Key? key, required this.listHistory}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: double.infinity,
          padding: const EdgeInsets.all(3),
          decoration: BoxDecoration(
              color: const Color.fromARGB(255, 25, 184, 181),
              borderRadius: BorderRadius.circular(2)),
          child: const Center(
              child: Text(
            'รายชื่อผู้รับรอง',
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 18, color: Colors.white),
          )),
        ),
        Container(
          width: double.infinity,
          height: 100,
          padding: const EdgeInsets.symmetric(horizontal: 5),
          decoration: BoxDecoration(
              color: Colors.white.withOpacity(0.9),
              borderRadius: BorderRadius.circular(2)),
          child: SingleChildScrollView(
              child: Column(
            children: listHistory.map((doc) {
              DocumentReference data = doc;
              return FutureBuilder(
                  future: Future.wait([data.get()]),
                  builder: ((BuildContext context, AsyncSnapshot list) {
                    if (list.hasData) {
                      DocumentReference certifier = list.data![0]['certifier'];
                      DocumentReference result = list.data![0]['result'];
                      return FutureBuilder(
                        future: Future.wait([certifier.get(), result.get()]),
                        builder: ((BuildContext context, AsyncSnapshot data) {
                          if (data.hasData) {
                            DocumentReference company = data.data[0]['company'];
                            return Container(
                              decoration: BoxDecoration(
                                color: Colors.cyan.shade200.withOpacity(0.8),
                                borderRadius: BorderRadius.circular(5),
                              ),
                              margin: const EdgeInsets.all(3),
                              child: IntrinsicHeight(
                                child: Row(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  children: [
                                    Container(
                                      margin: const EdgeInsets.all(5),
                                      decoration: BoxDecoration(
                                          color: const Color.fromARGB(
                                              189, 123, 136, 162),
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      child: const Icon(Icons.person,
                                          color: Colors.white, size: 60),
                                    ),
                                    Expanded(
                                      child: Container(
                                        margin: const EdgeInsets.symmetric(
                                            vertical: 5),
                                        child: Column(
                                          children: [
                                            Container(
                                              color: Colors.blueGrey,
                                              height: 10,
                                            ),
                                            Container(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 5,
                                                      vertical: 2),
                                              width: double.infinity,
                                              decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  borderRadius:
                                                      BorderRadius.circular(3)),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    'ผู้รับรอง : ${data.data[0]['name']}',
                                                    style: const TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold),
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                  ),
                                                  Text(
                                                    'วัน-เวลา : ${dateFormat.format(list.data[0]['date_time'].toDate()).toString()}',
                                                    style: const TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold),
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                  ),
                                                  FutureBuilder(
                                                    future: Future.wait(
                                                        [company.get()]),
                                                    builder:
                                                        (BuildContext context,
                                                            AsyncSnapshot
                                                                snapshot) {
                                                      if (snapshot.hasData) {
                                                        return Text(
                                                          'สังกัด : ${snapshot.data[0]['name']}',
                                                          style: const TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                        );
                                                      }
                                                      if (snapshot.hasError) {
                                                        return Center(
                                                            child: Text(snapshot
                                                                .error
                                                                .toString()));
                                                      }
                                                      return const Center(
                                                          child:
                                                              CircularProgressIndicator());
                                                    },
                                                  )
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: const EdgeInsets.all(5),
                                      padding: const EdgeInsets.all(5),
                                      decoration: BoxDecoration(
                                          color: list.data[0]['approve']
                                              ? const Color.fromARGB(
                                                  255, 70, 182, 93)
                                              : const Color.fromARGB(
                                                  255, 201, 53, 53),
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      child: Icon(
                                        list.data[0]['approve']
                                            ? Icons.done
                                            : Icons.clear,
                                        color: Colors.white,
                                        size: 25,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            );
                          }
                          if (data.hasError) {
                            return Center(child: Text(data.error.toString()));
                          }
                          return const Center(
                              child: CircularProgressIndicator());
                        }),
                      );
                    }
                    if (list.hasError) {
                      return Center(child: Text(list.error.toString()));
                    }
                    return const Center(child: CircularProgressIndicator());
                  }));
            }).toList(),
          )),
        ),
      ],
    );
  }
}
