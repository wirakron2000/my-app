import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

CollectionReference atkRef = FirebaseFirestore.instance.collection('atk_list');
String? currentAtk;

Future<void> addAtk(String atkBrand) async {
  await atkRef
      .add({'brand': atkBrand})
      .then((value) => debugPrint('Success to add atk brand.'))
      .catchError((error) => debugPrint('Failed to add atk brand: $error'));
}

Future getAtk(String id) async {
  try {
    await atkRef.doc(id).get().then((value) {
      debugPrint(value.get('brand'));
      debugPrint('Success to get atk brand.');
    });
  } catch (e) {
    debugPrint('Failed to get atk: $e');
  }
}

Future getAllAtk() async {
  try {
    return await atkRef.get();
  } catch (e) {
    debugPrint('Failed to get all atk: $e');
  }
}

Future<void> updateAtk(String id, String brand) async {
  await atkRef
      .doc(id)
      .update({
        'brand': brand,
      })
      .then((value) => debugPrint('Success to delete atk brand.'))
      .catchError((error) => debugPrint('Failed to delete atk brand: $error'));
}

Future<void> deleteAtk(String id) async {
  await atkRef
      .doc(id)
      .delete()
      .then((value) => debugPrint('Success to delete atk brand'))
      .catchError((error) => debugPrint('Failed to delete atk brand: $error'));
}

Stream<QuerySnapshot> getAtkStream() {
  return atkRef.orderBy('brand', descending: false).snapshots();
}

String getAtkPath(String id) {
  return atkRef.doc(id).path;
}
