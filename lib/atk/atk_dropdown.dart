import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:my_app/export_file/service.dart';

class DropDownAtk extends StatefulWidget {
  const DropDownAtk({Key? key}) : super(key: key);

  @override
  State<DropDownAtk> createState() => _DropDownAtkState();
}

class _DropDownAtkState extends State<DropDownAtk> {
  String? _dropdownValue;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: getAtkStream(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return const Text('Something went wrong');
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Text('Loading..');
          }
          return Center(
            child: SizedBox(
              width: double.infinity,
              child: DropdownButton(
                isExpanded: true,
                hint: const Text('กรุณาเลือกยี่ห้อ ATK'),
                style: const TextStyle(color: Colors.black),
                icon: const Icon(Icons.arrow_drop_down,
                    size: 30, color: Colors.grey),
                value: _dropdownValue,
                items: snapshot.data!.docs.map((DocumentSnapshot atk) {
                  return DropdownMenuItem<String>(
                    value: atk.id,
                    child: Text(
                      atk.get('brand'),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    ),
                  );
                }).toList(),
                onChanged: (String? newValue) {
                  setState(() {
                    if (newValue != null) {
                      _dropdownValue = newValue;
                      currentAtk = newValue;
                    }
                    debugPrint('Current Atk Brand: $currentAtk');
                  });
                },
              ),
            ),
          );
        });
  }
}
