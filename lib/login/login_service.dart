import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:my_app/export_file/service.dart';

GoogleSignIn googleSignIn = GoogleSignIn(scopes: <String>['email']);

Future<void> googleLogin() async {
  try {
    final GoogleSignInAccount? user = await googleSignIn.signIn();

    final GoogleSignInAuthentication? googleAuth = await user?.authentication;

    if (googleAuth?.accessToken != null && googleAuth?.idToken != null) {
      final credential = GoogleAuthProvider.credential(
        accessToken: googleAuth?.accessToken,
        idToken: googleAuth?.idToken,
      );
      UserCredential userCredential = await FirebaseAuth.instance
          .signInWithCredential(credential)
          .whenComplete(() {
        debugPrint(user!.email);
        debugPrint(user.displayName.toString());
      });

      if (userCredential.user != null) {
        if (userCredential.additionalUserInfo!.isNewUser) {
          debugPrint('new user');
          addUser();
        } else {
          debugPrint('old user');
        }
      }
    }
  } on FirebaseAuthException catch (e) {
    debugPrint('Failed to login: $e');
  }
}

Future<void> logOut() async {
  try {
    await FirebaseAuth.instance.signOut();
    await googleSignIn.signOut();
    currentUser = null;
  } on FirebaseAuthException catch (e) {
    debugPrint('Failed to log out: $e');
  }
}

Future<void> logOutDialog(BuildContext context) async {
  showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text('ออกจากระบบ'),
        content: SingleChildScrollView(
          child: ListBody(
            children: const <Widget>[
              Text('คุณแน่ใจแล้วใช่ไหม? ที่จะทำการออกจากระบบ.'),
              //Text('Would you like to approve of this message?'),
            ],
          ),
        ),
        actions: <Widget>[
          TextButton(
            child: const Text('ออกจากระบบ'),
            onPressed: () async {
              await logOut().then((value) => Navigator.of(context)
                  .pushNamedAndRemoveUntil('/login', (route) => false));
            },
          ),
          TextButton(
            child: const Text('ยกเลิก'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
