import 'package:flutter/material.dart';
import 'package:my_app/export_file/service.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          padding: const EdgeInsets.all(20),
          constraints: BoxConstraints(
            maxHeight: MediaQuery.of(context).size.height,
            maxWidth: MediaQuery.of(context).size.width,
          ),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Colors.blueAccent.withOpacity(0.80),
                Colors.lightBlueAccent.withOpacity(0.70)
              ],
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                flex: 2,
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 36.0, horizontal: 24.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      Text(
                        "Sign-in/Login",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 40,
                          fontWeight: FontWeight.w800,
                          fontFamily: 'Kanit',
                        ),
                      ),
                      SizedBox(height: 5),
                      Text(
                        "ATK Passport App",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 25,
                          fontWeight: FontWeight.w300,
                          fontFamily: 'Kanit',
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 7,
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.35),
                      borderRadius:
                          const BorderRadius.all(Radius.circular(40))),
                  child: Center(
                    child: GestureDetector(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            width: 175,
                            height: 175,
                            decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.35),
                              borderRadius: BorderRadius.circular(150),
                            ),
                            child: Image.network(
                                'http://pngimg.com/uploads/google/google_PNG19635.png',
                                width: 175,
                                height: 175,
                                alignment: Alignment.bottomCenter),
                          ),
                          Container(
                              margin: const EdgeInsets.fromLTRB(10, 15, 10, 5),
                              child: const Text('LOGIN WITH GOOGLE',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.black38,
                                      fontWeight: FontWeight.w700,
                                      fontSize: 20))),
                          const SizedBox(height: 100)
                        ],
                      ),
                      onTap: () async {
                        try {
                          await googleLogin();
                          if (!mounted) return;
                          Navigator.of(context).pushNamedAndRemoveUntil(
                              '/home', (route) => false);
                        } catch (e) {
                          debugPrint(e.toString());
                          Navigator.pop(context);
                        }
                      },
                    ),
                  ),
                ),
              ),
            ],
          )),
    );
  }
}
