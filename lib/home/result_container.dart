import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:my_app/export_file/service.dart';
import 'package:my_app/export_file/widget.dart';

class ResultContainer extends StatefulWidget {
  const ResultContainer({Key? key}) : super(key: key);

  @override
  State<ResultContainer> createState() => _ResultContainerState();
}

class _ResultContainerState extends State<ResultContainer> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 5),
      decoration: BoxDecoration(
          border: Border.all(
            color: Colors.white54,
            width: 3,
          ),
          borderRadius: BorderRadius.circular(5)),
      child: Column(
        children: [
          Container(
            margin: const EdgeInsets.all(5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: const EdgeInsets.all(5),
                  decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.6),
                      borderRadius: const BorderRadius.all(Radius.circular(3))),
                  child: const Text(
                    'ผลการบันทึกล่าสุด',
                    style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                  ),
                ),
                GestureDetector(
                  child: Container(
                    padding: const EdgeInsets.all(5),
                    decoration: const BoxDecoration(
                        color: Color.fromARGB(255, 69, 196, 194),
                        borderRadius: BorderRadius.all(Radius.circular(5))),
                    child: Row(
                      children: const [
                        Icon(
                          Icons.post_add_rounded,
                          color: Colors.black54,
                        ),
                        SizedBox(width: 2),
                        Center(
                          child: Text('เพิ่มบันทึกผล ATK',
                              style: TextStyle(
                                  color: Colors.black54,
                                  fontWeight: FontWeight.bold)),
                        ),
                      ],
                    ),
                  ),
                  onTap: () async {
                    await getImage().then((value) {
                      Navigator.of(context).pushNamed('/add_result');
                    });
                  },
                )
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(5, 0, 5, 5),
            width: double.infinity,
            height: 150,
            decoration: BoxDecoration(color: Colors.white.withOpacity(0.45)),
            child: StreamBuilder(
              stream: getNewestResultStream(),
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.data?.size == 0) {
                  return const Center(
                    child: Text('ไม่มีผลการบันทึก ATK'),
                  );
                }
                if (snapshot.hasError) {
                  return const Text('Something went wrong');
                }
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Container(
                    margin: const EdgeInsets.symmetric(
                        horizontal: 100, vertical: 10),
                    child: const CircularProgressIndicator(strokeWidth: 5),
                  );
                }
                return CustomScrollView(
                  slivers: [
                    SliverGrid(
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2),
                      delegate: SliverChildListDelegate(snapshot.data!.docs.map(
                        (DocumentSnapshot doc) {
                          return BoxResult(data: doc);
                        },
                      ).toList()),
                    )
                  ],
                );
              },
            ),
          ),
          GestureDetector(
            child: Container(
                margin: const EdgeInsets.fromLTRB(5, 0, 5, 5),
                padding: const EdgeInsets.all(5),
                width: double.infinity,
                decoration:
                    const BoxDecoration(color: Color.fromARGB(99, 75, 70, 70)),
                child: const Text('ดูผลการบันทึกทั้งหมด',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.w700))),
            onTap: () {
              Navigator.of(context)
                  .pushNamedAndRemoveUntil('/all_result', (route) => false);
            },
          )
        ],
      ),
    );
  }
}
