import 'package:flutter/material.dart';
import 'package:my_app/export_file/service.dart';
import 'package:my_app/export_file/widget.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    setState(() {
      setCurrentUser();
    });
    setState(() {
      getCompany();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: const CustomNavbar(),
        backgroundColor: Colors.blueAccent,
        appBar: AppBar(
          backgroundColor: const Color.fromARGB(255, 69, 196, 194),
          centerTitle: true,
          title: const Text(
            'หน้าจอหลัก',
            style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
          ),
          actions: [
            GestureDetector(
              child: Container(
                margin: const EdgeInsets.all(5),
                padding: const EdgeInsets.only(left: 7, right: 3),
                decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.2),
                    borderRadius: const BorderRadius.all(Radius.circular(5))),
                child: Row(
                  children: const [
                    Center(
                      child: Text('LOG\nOUT',
                          style: TextStyle(fontWeight: FontWeight.bold)),
                    ),
                    SizedBox(width: 2),
                    Icon(Icons.logout, size: 35),
                  ],
                ),
              ),
              onTap: () async {
                logOutDialog(context);
              },
            ),
          ],
        ),
        body: Container(
          padding: const EdgeInsets.all(5),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Colors.blueAccent.withOpacity(0.80),
                Colors.lightBlueAccent.withOpacity(0.70)
              ],
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
            ),
          ),
          child: SingleChildScrollView(
            child: Container(
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.45),
                  borderRadius: const BorderRadius.all(Radius.circular(10))),
              child: Column(
                children: const [
                  ProfileContainer(),
                  ResultContainer(),
                  EntryHistoryContainer(),
                ],
              ),
            ),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton.large(
          backgroundColor:
              const Color.fromARGB(150, 59, 164, 162).withOpacity(0.8),
          child: const Icon(Icons.qr_code_scanner_outlined, size: 65),
          onPressed: () {
            qrNavi = 'result';
            Navigator.of(context).pushNamed('/qr_scan');
            debugPrint('Qr scan');
          },
        ),
        bottomNavigationBar: customBottomBar(context),
      ),
    );
  }
}
