import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:my_app/export_file/service.dart';

class EntryHistoryContainer extends StatefulWidget {
  const EntryHistoryContainer({Key? key}) : super(key: key);

  @override
  State<EntryHistoryContainer> createState() => _EntryHistoryContainerState();
}

class _EntryHistoryContainerState extends State<EntryHistoryContainer> {
  @override
  void initState() {
    setState(() {});
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 5),
      decoration: BoxDecoration(
          border: Border.all(
            color: Colors.white54,
            width: 3,
          ),
          borderRadius: BorderRadius.circular(5)),
      child: Column(
        children: [
          Container(
            margin: const EdgeInsets.all(5),
            padding: const EdgeInsets.all(5),
            width: double.infinity,
            decoration: BoxDecoration(
                color: Colors.white.withOpacity(0.6),
                borderRadius: const BorderRadius.all(Radius.circular(3))),
            child: const Text(
              'ประวัติการเข้าสถานที่ล่าสุด',
              style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(5, 0, 5, 5),
            width: double.infinity,
            height: 150,
            decoration: BoxDecoration(color: Colors.white.withOpacity(0.45)),
            child: StreamBuilder(
              stream: getEntryHistoryStream(),
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                if (snapshot.data?.size == 0) {
                  return const Center(
                    child: Text('ไม่มีประวัติการเข้าสถานที่'),
                  );
                }
                if (snapshot.hasError) {
                  return const Text('Something went wrong',
                      textAlign: TextAlign.center);
                }
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Container(
                    margin: const EdgeInsets.symmetric(
                        horizontal: 100, vertical: 10),
                    child: const CircularProgressIndicator(strokeWidth: 5),
                  );
                }
                return CustomScrollView(
                  slivers: [
                    SliverGrid(
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2),
                      delegate: SliverChildListDelegate(snapshot.data!.docs.map(
                        (DocumentSnapshot doc) {
                          return Container();
                        },
                      ).toList()),
                    )
                  ],
                );
              },
            ),
          ),
          GestureDetector(
            child: Container(
                margin: const EdgeInsets.fromLTRB(5, 0, 5, 5),
                padding: const EdgeInsets.all(5),
                width: double.infinity,
                decoration:
                    const BoxDecoration(color: Color.fromARGB(99, 75, 70, 70)),
                child: const Text('ดูประวัติการเข้าสถานที่ทั้งหมด',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.w700))),
            onTap: () {
              Navigator.of(context)
                  .pushNamedAndRemoveUntil('/entry_history', (route) => false);
            },
          )
        ],
      ),
    );
  }
}
