import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:my_app/export_file/service.dart';

class CustomNavbar extends StatelessWidget {
  const CustomNavbar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(
                  5, MediaQuery.of(context).padding.top + 10, 10, 15),
              color: const Color.fromARGB(255, 69, 196, 194),
              child: SizedBox(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.40),
                          borderRadius: BorderRadius.circular(180)),
                      child: ClipOval(
                        child: CachedNetworkImage(
                            width: 75,
                            height: 75,
                            placeholder: (context, url) =>
                                const CircularProgressIndicator(),
                            errorWidget: (context, url, error) =>
                                const Icon(Icons.error),
                            imageUrl: currentUser!.photoURL.toString()),
                      ),
                    ),
                    const SizedBox(width: 5),
                    Expanded(
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 10, horizontal: 5),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              margin: EdgeInsets.zero,
                              child: Text(
                                currentUser!.displayName.toString(),
                                style: const TextStyle(
                                    fontSize: 20, color: Colors.white),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            const SizedBox(height: 15),
                            Container(
                              margin: EdgeInsets.zero,
                              child: Text(
                                currentUser!.email.toString(),
                                style: const TextStyle(
                                    fontSize: 20, color: Colors.white),
                                maxLines: 1,
                                textWidthBasis: TextWidthBasis.parent,
                                overflow: TextOverflow.ellipsis,
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(5),
              child: Wrap(
                runSpacing: 10,
                children: [
                  ListTile(
                    leading: const Icon(Icons.home, size: 30),
                    title: const Text('หน้าจอหลัก',
                        style: TextStyle(fontSize: 20)),
                    onTap: () {
                      Navigator.of(context)
                          .pushNamedAndRemoveUntil('/home', (route) => false);
                    },
                  ),
                  ListTile(
                    leading: const Icon(Icons.account_box, size: 30),
                    title: const Text('บัญชีผู้ใช้',
                        style: TextStyle(fontSize: 20)),
                    onTap: () {
                      Navigator.of(context).pushNamedAndRemoveUntil(
                          '/profile', (route) => false);
                    },
                  ),
                  ListTile(
                    leading: const Icon(Icons.post_add_rounded, size: 30),
                    title: const Text('เพิ่มบันทึกผล ATK',
                        style: TextStyle(fontSize: 20)),
                    onTap: () async {
                      await getImage().then((value) {
                        Navigator.of(context).pushNamed('/add_result');
                      });
                    },
                  ),
                  ListTile(
                    leading: const Icon(Icons.article_outlined, size: 30),
                    title: const Text('ประวัติผลบันทึก ATK',
                        style: TextStyle(fontSize: 20)),
                    onTap: () async {
                      Navigator.of(context).pushNamedAndRemoveUntil(
                          '/all_result', (route) => false);
                    },
                  ),
                  ListTile(
                    leading: const Icon(Icons.inventory_outlined, size: 30),
                    title: const Text('ประวัติการเข้าสถานที่',
                        style: TextStyle(fontSize: 20)),
                    onTap: () async {
                      Navigator.of(context).pushNamedAndRemoveUntil(
                          '/entry_history', (route) => false);
                    },
                  ),
                  if (getHasCompany())
                    ListTile(
                      leading: const Icon(Icons.business_outlined, size: 30),
                      title: const Text('สถานที่ตรวจสอบ',
                          style: TextStyle(fontSize: 20)),
                      onTap: () {
                        Navigator.of(context).pushNamedAndRemoveUntil(
                            '/locations', (route) => false);
                      },
                    ),
                  const Divider(color: Colors.black, height: 5),
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.grey.withOpacity(0.3),
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10))),
                    child: ListTile(
                      leading: const Icon(Icons.logout, size: 30),
                      title: const Text('ออกจากระบบ',
                          style: TextStyle(fontSize: 20)),
                      onTap: () {
                        logOutDialog(context);
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

customBottomBar(BuildContext context) => BottomAppBar(
      shape: const CircularNotchedRectangle(),
      notchMargin: 5,
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            IconButton(
              tooltip: 'หน้าจอหลัก',
              icon: const Icon(Icons.home, color: Colors.blueGrey, size: 30),
              onPressed: () {
                Navigator.of(context)
                    .pushNamedAndRemoveUntil('/home', (route) => false);
              },
            ),
            IconButton(
              tooltip: 'บัญชีผู้ใช้ ',
              icon: const Icon(Icons.account_box,
                  color: Colors.blueGrey, size: 30),
              onPressed: () {
                Navigator.of(context)
                    .pushNamedAndRemoveUntil('/profile', (route) => false);
              },
            ),
            const SizedBox(width: 60),
            IconButton(
              tooltip: 'ประวัติผลบันทึก ATK',
              icon: const Icon(Icons.article_outlined,
                  color: Colors.blueGrey, size: 30),
              onPressed: () {
                Navigator.of(context)
                    .pushNamedAndRemoveUntil('/all_result', (route) => false);
              },
            ),
            IconButton(
              tooltip: 'ประวัติการเข้าสถานที่',
              icon: const Icon(Icons.inventory_outlined,
                  color: Colors.blueGrey, size: 30),
              onPressed: () {
                Navigator.of(context).pushNamedAndRemoveUntil(
                    '/entry_history', (route) => false);
              },
            ),
          ],
        ),
      ),
    );
