import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:my_app/export_file/service.dart';

class BoxResult extends StatelessWidget {
  final DocumentSnapshot? data;
  const BoxResult({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    DocumentReference refAtk = data?.get('atk_brand');
    DocumentReference refUser = data?.get('name');
    return GestureDetector(
      child: FutureBuilder(
          future: Future.wait([refAtk.get(), refUser.get()]),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return Container(
                  decoration: BoxDecoration(
                      color: const Color.fromARGB(255, 69, 196, 194)
                          .withOpacity(0.6),
                      borderRadius: const BorderRadius.all(Radius.circular(3))),
                  margin: const EdgeInsets.all(5),
                  padding: const EdgeInsets.all(5),
                  child: Container(
                    width: double.infinity,
                    padding: const EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.6),
                        borderRadius:
                            const BorderRadius.all(Radius.circular(3))),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SizedBox(
                              width: 50,
                              child: CachedNetworkImage(
                                placeholder: (context, url) =>
                                    const CircularProgressIndicator(),
                                errorWidget: (context, url, error) =>
                                    const Icon(Icons.error),
                                imageUrl: data?.get('image')! != null
                                    ? data?.get('image')
                                    : '',
                              ),
                            ),
                            const SizedBox(width: 3),
                            Expanded(
                              child: Column(
                                children: [
                                  Container(
                                    width: double.infinity,
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5),
                                    decoration: const BoxDecoration(
                                        color: Color.fromARGB(242, 33, 30, 71)),
                                    child: const Text(
                                      'ยี่ห้อ Atk',
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                  Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5),
                                    decoration: BoxDecoration(
                                        color: Colors.white.withOpacity(0.6)),
                                    child: Text(
                                      snapshot.data[0]['brand'],
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 3,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          width: double.infinity,
                          child: Text('วันเวลาที่บันทึก:',
                              overflow: TextOverflow.ellipsis, maxLines: 1),
                        ),
                        SizedBox(
                          width: double.infinity,
                          child: Text(
                              data?.get('date_time') != null
                                  ? dateFormat
                                      .format(data!.get('date_time')!.toDate())
                                  : 'error',
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1),
                        ),
                        //const SizedBox(),
                        const Divider(height: 5, color: Colors.black),
                        const Center(
                          child: Text('กดเพื่อดูรายละเอียด',
                              overflow: TextOverflow.ellipsis, maxLines: 1),
                        ),
                      ],
                    ),
                  ));
            } else {
              return Container(
                padding: const EdgeInsets.all(50),
                width: 10,
                height: 10,
                child: const CircularProgressIndicator(
                  strokeWidth: 5,
                ),
              );
            }
          }),
      onTap: () {
        resultPath = data?.reference.path.toString();
        newResult = false;
        Navigator.of(context).pushNamed('/add_entry');
      },
    );
  }
}
