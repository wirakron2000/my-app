import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:my_app/export_file/service.dart';

Position? position;
Placemark? place;

getCurrentPosition() async {
  Position currentPosition = await determinePosition();
  position = currentPosition;
  getPlacemarks(position?.latitude, position?.longitude);
}

getPlacemarks(lat, log) async {
  List<Placemark> placemarks = await placemarkFromCoordinates(lat, log);
  place = placemarks[0];
  address.text = getAddress();
}

String getAddress() {
  return '${place?.street}, ${place?.locality}, ${place?.subAdministrativeArea}, ${place?.administrativeArea}, ${place?.country}, ${place?.postalCode}.';
}

void clearPosition() {
  position = null;
  place = null;
}

Future<Position> determinePosition() async {
  LocationPermission permission;
  permission = await Geolocator.checkPermission();
  if (permission == LocationPermission.denied) {
    permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.denied) {
      return Future.error('Location permissions are denied');
    }
  }
  return await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high);
}
