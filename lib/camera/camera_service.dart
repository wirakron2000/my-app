import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';

File? imageFile;
String? currentDate;
String? fileFormat;
File? newPath;

Future getImage() async {
  final image = await ImagePicker().pickImage(source: ImageSource.camera);
  if (image == null) return;

  final imgTemp = File(image.path);
  final directory = await getApplicationDocumentsDirectory();
  fileFormat = imgTemp.path.split('.').last;
  // currentDate = DateFormat('dMMy').format(DateTime.now());
  // newPath = await imgTemp.copy(
  //   '${directory.path}/result_$currentDate.$fileFormat',
  // );
  int currentMil = DateTime.now().millisecondsSinceEpoch;
  newPath = await imgTemp.copy(
    '${directory.path}/result$currentMil.$fileFormat',
  );
  imageFile = newPath;
}
