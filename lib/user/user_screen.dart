import 'package:flutter/material.dart';
import 'package:my_app/export_file/screen.dart';
import 'package:my_app/export_file/service.dart';
import 'package:my_app/export_file/widget.dart';

class UserScreen extends StatefulWidget {
  const UserScreen({Key? key}) : super(key: key);

  @override
  State<UserScreen> createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> {
  @override
  void initState() {
    setCurrentUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (currentUser == null) {
      Navigator.pop(context);
    }
    return Scaffold(
      drawer: const CustomNavbar(),
      backgroundColor: Colors.blueAccent,
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 69, 196, 194),
        centerTitle: true,
        title: const Text(
          'บัญชีผู้ใช้ ',
          style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
        ),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Colors.blueAccent.withOpacity(0.80),
              Colors.lightBlueAccent.withOpacity(0.70)
            ],
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
          ),
        ),
        padding: const EdgeInsets.all(20),
        child: currentUser != null
            ? Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Center(
                    child: ClipOval(
                      child: Image.network(currentUser!.photoURL.toString()),
                    ),
                  ),
                  Center(
                      child: Text(currentUser!.email.toString(),
                          style: const TextStyle(
                            fontSize: 30,
                            color: Colors.white,
                          ))),
                  Center(
                      child: Text(currentUser!.displayName.toString(),
                          style: const TextStyle(
                            fontSize: 30,
                            color: Colors.white,
                          ))),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Center(
                        child: GestureDetector(
                          child: Container(
                              padding: const EdgeInsets.all(20),
                              decoration: BoxDecoration(
                                  color:
                                      Colors.deepPurpleAccent.withOpacity(0.5),
                                  borderRadius: BorderRadius.circular(20)),
                              child: const Text('HOME',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w700,
                                      fontSize: 20))),
                          onTap: () async {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const HomeScreen()));
                          },
                        ),
                      ),
                      Center(
                        child: GestureDetector(
                          child: Container(
                              padding: const EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                  color:
                                      Colors.deepPurpleAccent.withOpacity(0.5),
                                  borderRadius: BorderRadius.circular(20)),
                              child: const Icon(
                                Icons.camera_alt_outlined,
                                color: Colors.white,
                                size: 50,
                              )),
                          onTap: () async {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const HomeScreen()));
                          },
                        ),
                      ),
                      Center(
                        child: GestureDetector(
                          child: Container(
                              padding: const EdgeInsets.all(20),
                              decoration: BoxDecoration(
                                  color:
                                      Colors.deepPurpleAccent.withOpacity(0.5),
                                  borderRadius: BorderRadius.circular(20)),
                              child: const Text('LOG OUT',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w700,
                                      fontSize: 20))),
                          onTap: () async {
                            await logOut();
                            if (!mounted) return;
                            Navigator.of(context).pushNamedAndRemoveUntil(
                                '/login', (route) => false);
                          },
                        ),
                      ),
                    ],
                  ),
                  const DropDownAtk(),
                  // SingleChildScrollView(
                  //   child: Column(
                  //     children: [
                  //       Container(
                  //         height: 100,
                  //         decoration: BoxDecoration(
                  //           color: Colors.greenAccent.withOpacity(0.5),
                  //           borderRadius: BorderRadius.circular(10),
                  //           border: Border.all(color: Colors.black, width: 2),
                  //         ),
                  //         child: ListView(
                  //           padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                  //           shrinkWrap: true,
                  //           //physics: const NeverScrollableScrollPhysics(),
                  //           children: <Widget>[
                  //             StreamBuilder(
                  //                 stream: getAtkStream(),
                  //                 builder: (BuildContext context,
                  //                     AsyncSnapshot<QuerySnapshot> snapshot) {
                  //                   if (snapshot.hasError) {
                  //                     return const Text('Something went wrong');
                  //                   }
                  //                   if (snapshot.connectionState ==
                  //                       ConnectionState.waiting) {
                  //                     return const Text('Loading..');
                  //                   }
                  //                   return ListBody(
                  //                     children: snapshot.data!.docs.map((data) {
                  //                       return Text(
                  //                           data.get('brand').toString());
                  //                     }).toList(),
                  //                   );
                  //                 })
                  //           ],
                  //         ),
                  //       )
                  //     ],
                  //   ),
                  // ),
                  Center(
                    child: GestureDetector(
                      child: Container(
                          margin: const EdgeInsets.fromLTRB(10, 5, 10, 5),
                          padding: const EdgeInsets.all(20),
                          decoration: BoxDecoration(
                              color: Colors.blueGrey.withOpacity(0.5),
                              borderRadius: BorderRadius.circular(30)),
                          child: const Text('QR-CODE',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 20))),
                      onTap: () async {
                        setState(() {});
                        showDialog(
                          context: context,
                          builder: (BuildContext context) => const QrCode(),
                        );
                      },
                    ),
                  ),
                ],
              )
            : Container(),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton.large(
        backgroundColor:
            const Color.fromARGB(150, 59, 164, 162).withOpacity(0.8),
        child: const Icon(Icons.qr_code_scanner_outlined, size: 65),
        onPressed: () {
          debugPrint('Qr scan');
        },
      ),
      bottomNavigationBar: customBottomBar(context),
    );
  }
}
