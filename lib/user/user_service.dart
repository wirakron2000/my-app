import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:my_app/export_file/service.dart';

CollectionReference userRef = FirebaseFirestore.instance.collection('user');
String? userPath;
User? currentUser = FirebaseAuth.instance.currentUser;

Future<void> addUser() async {
  await userRef
      .doc(googleSignIn.currentUser?.email)
      .set({
        'name': googleSignIn.currentUser?.displayName.toString(),
        'email': googleSignIn.currentUser?.email,
        'register_date': FieldValue.serverTimestamp(),
        'type': 'ผู้ใช้งานทั่วไป',
      })
      .then((value) => debugPrint('Success to add User.'))
      .catchError((error) => debugPrint('Failed to add User: $error'));
}

Future<void> getUser(String id) async {
  try {
    await userRef.doc(id).get().then((value) {
      debugPrint(value.get('name'));
    });
  } catch (e) {
    debugPrint('Failed to get user: $e');
  }
}

Future getAllUser() async {
  try {
    return await userRef.get();
  } catch (e) {
    debugPrint('Failed to get all user: $e');
    return null;
  }
}

Future<void> updateUser(String id, String name) async {
  await userRef
      .doc(id)
      .update({
        'name': name,
      })
      .then((value) => debugPrint('Success to update user.'))
      .catchError((error) => debugPrint('Failed to update User: $error'));
}

Future<void> deleteUser(String id) async {
  await userRef
      .doc(id)
      .delete()
      .then((value) => debugPrint('Success to delete user'))
      .catchError((error) => debugPrint('Failed to delete User: $error'));
}

Stream<QuerySnapshot> getUserStream() {
  return userRef.orderBy('name', descending: false).snapshots();
}

String getUserPath(String id) {
  return userRef.doc(id).path;
}

void setCurrentUser() {
  currentUser = FirebaseAuth.instance.currentUser;
}
