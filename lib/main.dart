import 'package:firebase_app_check/firebase_app_check.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:my_app/export_file/screen.dart';
import 'package:my_app/export_file/service.dart';
import 'firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  await FirebaseAppCheck.instance.activate();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Project Application.',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: currentUser != null ? '/home' : '/login',
      routes: {
        '/home': (context) => const HomeScreen(),
        '/login': (context) => const LoginScreen(),
        '/result': (context) => const ResultScreen(),
        '/add_result': (context) => const AddResultScreen(),
        '/profile': (context) => const UserScreen(),
        '/camera': (context) => const CameraScreen(),
        '/qr_scan': (context) => const QrScanScreen(),
        '/verify_result': (context) => const VerifyResultScreen(),
        '/add_entry': (context) => const AddEntryHistoryScreen(),
        '/locations': (context) => const LocationPage(),
        '/all_result': (context) => const AllResultScreen(),
        '/entry_history': (context) => const EntryHistoryScreen(),
      },
    );
  }
}
