import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

String? qrPath;
Barcode? result;
String? qrNavi;
QRViewController? qrController;
final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

setQrPath(String ref) {
  qrPath = ref;
}

clearQrPath() {
  qrPath = null;
}
