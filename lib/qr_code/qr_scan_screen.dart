import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:my_app/export_file/service.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class QrScanScreen extends StatefulWidget {
  const QrScanScreen({Key? key}) : super(key: key);

  @override
  State<QrScanScreen> createState() => _QrScanScreenState();
}

class _QrScanScreenState extends State<QrScanScreen> {
  @override
  void initState() {
    setState(() {});
    super.initState();
  }

  @override
  void reassemble() {
    super.reassemble();
    setState(() {
      if (Platform.isAndroid) {
        qrController!.pauseCamera();
      }
      qrController!.resumeCamera();
    });
  }

  @override
  void dispose() {
    qrController?.dispose();
    super.dispose();
  }

  void _onQRViewCreated(QRViewController controller) {
    setState(() {
      qrController = controller;
    });
    String path = '';
    controller.scannedDataStream.listen((scanData) {
      setState(() {
        result = scanData;
        debugPrint(result?.code.toString());
      });
      if (qrNavi == 'result') {
        setState(() {
          resultPath = result?.code;
          path = '/verify_result';
        });
      }
      if (qrNavi == 'entry') {
        setState(() {
          userPath = result?.code;
          path = '/entry_history';
        });
      }
      if (qrNavi == 'verify') {
        setState(() {
          resultPath = result?.code;
          path = '/verrify_profile';
        });
      }
      controller.stopCamera();
      Navigator.of(context).pushNamedAndRemoveUntil(path, (route) => false);
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: true,
          backgroundColor: const Color.fromARGB(255, 69, 196, 194),
          centerTitle: true,
          title: const Text(
            'แสกน QR-code',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
        ),
        body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          margin: const EdgeInsets.all(0),
          child: Column(
            children: <Widget>[
              Expanded(flex: 2, child: _buildQrView(context)),
              Container(
                margin: const EdgeInsets.all(8),
                child: ElevatedButton(
                    onPressed: () async {
                      await qrController?.flipCamera();
                      setState(() {});
                    },
                    child: FutureBuilder(
                      future: qrController?.getCameraInfo(),
                      builder: (context, snapshot) {
                        if (snapshot.data != null) {
                          return Text(
                              'Camera facing ${describeEnum(snapshot.data!)}');
                        } else {
                          return const Text('loading');
                        }
                      },
                    )),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildQrView(BuildContext context) {
    var scanArea = (MediaQuery.of(context).size.width < 400 ||
            MediaQuery.of(context).size.height < 400)
        ? 250.0
        : 450.0;
    return QRView(
      key: qrKey,
      onQRViewCreated: _onQRViewCreated,
      overlay: QrScannerOverlayShape(
          borderColor: Colors.blueAccent,
          borderRadius: 10,
          borderLength: 30,
          borderWidth: 10,
          cutOutSize: scanArea),
      onPermissionSet: (ctrl, p) => _onPermissionSet(context, ctrl, p),
    );
  }

  void _onPermissionSet(BuildContext context, QRViewController ctrl, bool p) {
    debugPrint('${DateTime.now().toIso8601String()}_onPermissionSet $p');
    if (!p) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('no Permission')),
      );
    }
  }
}
